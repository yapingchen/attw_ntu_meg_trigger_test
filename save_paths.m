%% if run first time on this system
% set up your path here
ptb_path = '/Applications/Psychtoolbox';
o_ptb_path = '/Users/b1064537/OneDrive - stud.sbg.ac.at/Arbeit/o_ptb';
home_path = '/Users/b1064537/OneDrive - stud.sbg.ac.at/Arbeit/exps/attw';

%% save your new path
hostname = matlab.lang.makeValidName(char(java.net.InetAddress.getLocalHost().getHostName()));
load o_ptb_paths
eval([hostname, '.ptb = ptb_path'])
eval([hostname, '.o_ptb = o_ptb_path'])
save('home_path.mat', 'home_path')
clear ptb o_ptb hostname ptb_path o_ptb_path home_path
save o_ptb_paths