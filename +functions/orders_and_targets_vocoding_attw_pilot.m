function orders_and_targets_vocoding_attw_pilot(subject_id)
%% get cfg
cfg = exp.init.prepare_cfg;
load(fullfile(cfg.data_path, subject_id));

%% create randomized array of audio and condition presentations
% ClockRandSeed();
% rng('shuffle');
nStim=24; % Anzahl der Gesamtstimuli
sound_order=1:nStim;
% sound_order=Shuffle(sound_order);

%% randomize condition order and avoid having a too long or too difficult block
nCond=24;
cond_order=1:nCond;
% cond_order=Shuffle(cond_order);
% cond_order_r = reshape(cond_order, 4, 6);
% cond_voc = mod(cond_order_r,3);
% cond_voc(cond_voc == 0) = 3;
% cond_voc_r = cond_voc;
% while sum(sum(cond_order_r)> 55) || sum(sum(cond_voc_r) < 7)
%     cond_order=Shuffle(cond_order);
%     cond_order_r = reshape(cond_order, 4, 6);
%     cond_voc = mod(cond_order_r,3);
%     cond_voc(cond_voc == 0) = 3;
%     cond_voc_r = cond_voc;
% end

%% organize random sounds
sound_path = fullfile(cfg.home_path, cfg.audiobook);
ind_15s=1; ind_30s=1; ind_60s=1; ind_90s=1; ind_120s=1; ind_150s=1;

for i=1:nStim
    if sound_order(i) <= 9 % for stimuli 15s
        sound_15s{ind_15s}=fullfile(sound_path, strcat(num2str(sound_order(i), '%02d'),'.wav')); %name accordingly
        ind_15s = ind_15s+1;
    elseif sound_order(i) > 9 && sound_order(i) <= 12 % for stimuli 30s
        sound_30s{ind_30s}=fullfile(sound_path, strcat(num2str(sound_order(i), '%02d'),'.wav')); %name accordingly
        ind_30s = ind_30s+1;
    elseif sound_order(i) > 12 && sound_order(i) <= 15 % for stimuli 60s
        sound_60s{ind_60s}=fullfile(sound_path, strcat(num2str(sound_order(i), '%02d'),'.wav')); %name accordingly
        ind_60s = ind_60s+1;
    elseif sound_order(i) > 15 && sound_order(i) <= 18 % for stimuli 90s
        sound_90s{ind_90s}=fullfile(sound_path, strcat(num2str(sound_order(i), '%02d'),'.wav')); %name accordingly
        ind_90s = ind_90s+1;
    elseif sound_order(i) > 18 && sound_order(i) <= 21 % for stimuli 120s
        sound_120s{ind_120s}=fullfile(sound_path, strcat(num2str(sound_order(i), '%02d'),'.wav')); %name accordingly
        ind_120s = ind_120s+1;
    elseif sound_order(i) > 21 && sound_order(i) <= 24 % for stimuli 150s
        sound_150s{ind_150s}=fullfile(sound_path, strcat(num2str(sound_order(i), '%02d'),'.wav')); %name accordingly
        ind_150s = ind_150s+1;
    end
end

%% target words
targetwords={'凳子', '歌唱', '口碑', '文章', '農夫', '碎片', '眼睛', '牆壁', '汙點', '飯碗', '饅頭', '老爺',... % 1-12
    '缺點', '神情', '異樣', '黃酒', '油燈', '決心', '主意', '道路', '聲音', '女人', '經驗', '俘虜'}; % 13-24

distractorwords={'兒子', '名人', '晚飯', '水田', '柵欄', '菜園', '舉人', '方法', '眼神', '敵人', '對手', '證據',...
    '外傳', '母親', '朋友', '新聞', '蘿蔔', '桑樹', '土牆', '竹筷', '志氣', '辮子', '底細', '結果'};
distractorwords = Shuffle(distractorwords);

%% random target sides
target_side=[ones(1, nStim/2), ones(1, nStim/2)*2];
target_side=Shuffle(target_side);

%% define condition order
order_path = fullfile(cfg.home_path, cfg.order);

subj_data.vocoding_attw_pilot.vocoding_order.sound_15s = sound_15s;
subj_data.vocoding_attw_pilot.vocoding_order.sound_30s = sound_30s;
subj_data.vocoding_attw_pilot.vocoding_order.sound_60s = sound_60s;
subj_data.vocoding_attw_pilot.vocoding_order.sound_90s = sound_90s;
subj_data.vocoding_attw_pilot.vocoding_order.sound_120s = sound_120s;
subj_data.vocoding_attw_pilot.vocoding_order.sound_150s = sound_150s;

subj_data.vocoding_attw_pilot.vocoding_order.sound_order = sound_order;
subj_data.vocoding_attw_pilot.vocoding_order.cond_order = cond_order;
subj_data.vocoding_attw_pilot.vocoding_order.target_side = target_side;
subj_data.vocoding_attw_pilot.vocoding_order.targetwords = targetwords;
subj_data.vocoding_attw_pilot.vocoding_order.distractorwords = distractorwords;

save(fullfile(cfg.data_path, subject_id), 'subj_data');





