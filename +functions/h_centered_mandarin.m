function [xp] = h_centered_mandarin(ptb, text)
% ptb.screen('TextFont', 'STSong');
[bbox, refbbox] = ptb.screen('TextBounds', double(text), 0, 0, 1, 0);
deltaboxX = refbbox(RectLeft) - bbox(RectLeft);
[~,dh] = CenterRect(bbox, ptb.win_rect);
xp = dh - deltaboxX;


