function vocoding_attw_trigger_test(subject_id, blk_n)
commandwindow;
%% get cfg
cfg = exp.init.prepare_cfg;

%% load subject_data
load(fullfile(cfg.data_path, subject_id));
MEG_trigger = subj_data.MEG_trigger;
MEG_mode = subj_data.MEG_trigger;

%% MEG trigger setting
if MEG_trigger
    ioObj1 = io64;
    ioObj2 = io64;
    status1 = io64(ioObj1);
    status2 = io64(ioObj2);
    lpt1 = hex2dec('7010');
    lpt2 = hex2dec('8010');
    io64(ioObj2,lpt2,0);
    
    % response pad trigger; check and modify before the exp.
    RH = 64;
    LH = 128;
end

%% init ptb
ptb_config = exp.init.config_ptb(MEG_mode);
ptb = exp.init.init_ptb(ptb_config);

% %% check if block has already been done...
% if blk_n <= subj_data.vocoding_attw_pilot_block_done
%     error('Participant already did that block');
% end %if
% 
% %% check if the block number is too high...
% if blk_n > subj_data.vocoding_attw_pilot_block_done + 1
%     fprintf('\n\n\nWARNING!!! You are skipping a block!! This is very probably NOT WHAT YOU WANT!\n');
%     fprintf('If you really want to continue, press the "y" button now.\n\n\n\n');
%     [~, key_pressed] = KbWait();
%     if find(key_pressed) ~= KbName('y')
%         return;
%     end %if
% end %if

%% say hello again...
fprintf('We should be done now...\n');

%% init screen and response
ptb.setup_screen;
ptb.setup_response;

%% get information of order & targets
if blk_n ==1
    functions.orders_and_targets_vocoding_attw_pilot(subject_id)
end
load(fullfile(cfg.data_path, subject_id));

%% allocate to variables
sound_order = subj_data.vocoding_attw_pilot.vocoding_order.sound_order;
nStim = length(sound_order);
cond_order = subj_data.vocoding_attw_pilot.vocoding_order.cond_order;
target_side = subj_data.vocoding_attw_pilot.vocoding_order.target_side;
targetwords = subj_data.vocoding_attw_pilot.vocoding_order.targetwords;
distractorwords = subj_data.vocoding_attw_pilot.vocoding_order.distractorwords;

sound_15s = subj_data.vocoding_attw_pilot.vocoding_order.sound_15s;
sound_30s = subj_data.vocoding_attw_pilot.vocoding_order.sound_30s;
sound_60s = subj_data.vocoding_attw_pilot.vocoding_order.sound_60s;
sound_90s = subj_data.vocoding_attw_pilot.vocoding_order.sound_90s;
sound_120s = subj_data.vocoding_attw_pilot.vocoding_order.sound_120s;
sound_150s = subj_data.vocoding_attw_pilot.vocoding_order.sound_150s;

if blk_n >1
%     ind_15s = subj_data.vocoding_attw_pilot.vocoding_order.ind_15s;
%     ind_30s = subj_data.vocoding_attw_pilot.vocoding_order.ind_30s;
%     ind_60s = subj_data.vocoding_attw_pilot.vocoding_order.ind_60s;
%     ind_90s = subj_data.vocoding_attw_pilot.vocoding_order.ind_90s;
%     ind_120s = subj_data.vocoding_attw_pilot.vocoding_order.ind_120s;
%     ind_150s = subj_data.vocoding_attw_pilot.vocoding_order.ind_150s;
    new_sound_array = subj_data.vocoding_attw_pilot.vocoding_order.new_sound_array;
    trg_start_log = subj_data.vocoding_attw_pilot.triggers.trg_start_log;
    trg_response_log = subj_data.vocoding_attw_pilot.triggers.trg_response_log;
    targetwords_log = subj_data.vocoding_attw_pilot.triggers.targetwords_log;
end
%% open log file
fid=fopen(strcat(fullfile(cfg.data_path, subject_id),'_vocode_attw_pilot.txt'),'A');
if blk_n == 1
    fprintf(fid,'%s','Trial');
    fprintf(fid,'\t%s','Condition');
    fprintf(fid,'\t%s','Stimuli');
    fprintf(fid,'\t%s','Trigger');
    fprintf(fid,'\t%s','Target');
    fprintf(fid,'\t%s','Distractor');
    fprintf(fid,'\t%s\n','Response');
end

%% load sound path
sound_path = fullfile(cfg.home_path, cfg.audiobook);

%% trial index
blk_total = 6;
trl_in_blk = nStim/blk_total; % nStim/blk_total = 24/6
ind_start = 1 +(blk_n-1)*trl_in_blk;
ind_end = ind_start + trl_in_blk - 1;

%% play instruction
instruction_1 = '測驗說明';
instruction_2 = '請仔細聆聽音檔並在音檔結束後，選出您最後聽到的名詞';
instruction_3 = '例如最後聽到的句子是:';
instruction_4 = '沉鈿鈿的將褲帶墜成了很彎很彎的弧線';
instruction_5 = '請選擇"弧線"作為正確答案';
instruction_6 = '請按任一鍵繼續';
ptb.screen('TextSize', 60);
ptb.screen('TextFont', 'STSong');
xp = functions.h_centered_mandarin(ptb, instruction_1);
ptb.screen('DrawText', double(instruction_1), xp, ptb.win_rect(4)/2-300);
xp = functions.h_centered_mandarin(ptb, instruction_2);
ptb.screen('DrawText', double(instruction_2), xp, ptb.win_rect(4)/2-100);
xp = functions.h_centered_mandarin(ptb, instruction_3);
ptb.screen('DrawText', double(instruction_3), xp, ptb.win_rect(4)/2);
xp = functions.h_centered_mandarin(ptb, instruction_4);
ptb.screen('DrawText', double(instruction_4), xp, ptb.win_rect(4)/2+100);
xp = functions.h_centered_mandarin(ptb, instruction_5);
ptb.screen('DrawText', double(instruction_5), xp, ptb.win_rect(4)/2+200);

ptb.screen('TextSize', 50);
xp = functions.h_centered_mandarin(ptb, instruction_6);
ptb.screen('DrawText', double(instruction_6), xp, ptb.win_rect(4)/2+340);
ptb.flip();

if MEG_trigger
    s = GetSecs; s0 = s; pressed=0;
    while (pressed~=RH) && (pressed~=LH) && (s-s0<1)
        s = GetSecs;
        pressed = io64(ioObj1,lpt1);
    end
    timestamp = ptb.flip();
    WaitSecs('UntilTime', timestamp + 2);
else
    ptb.wait_for_keys({'next'});
    timestamp = ptb.flip();
    WaitSecs('UntilTime', timestamp + 2);
end

%% vocode sounds
vocode_bw = [200 7000]; % sound band-width

% if blk_n ==1
%     ind_15s=1; ind_30s=1; ind_60s=1; ind_90s=1; ind_120s=1; ind_150s=1;
% end

for i= ind_start:ind_end
    stim_loading = sprintf('音檔準備中 %d/%d', i, ind_end);
    ptb.screen('TextSize', 80);
    xp = functions.h_centered_mandarin(ptb, stim_loading);
    ptb.screen('DrawText', double(stim_loading), xp, ptb.win_rect(4)/2);
    ptb.flip();
    
    % load sound path % modified for trigger test
    if cond_order(i) <= 9
        cur_wavpath=sound_15s{cond_order(i)};
%         ind_15s = ind_15s+1;
    elseif cond_order(i) > 9 && cond_order(i) <= 12
        cur_wavpath=sound_30s{cond_order(i)-9};
%         ind_30s = ind_30s+1;
    elseif cond_order(i) >12 && cond_order(i) <= 15
        cur_wavpath=sound_60s{cond_order(i)-12};
%         ind_60s = ind_60s+1;
    elseif cond_order(i) >15 && cond_order(i) <= 18
        cur_wavpath=sound_90s{cond_order(i)-15};
%         ind_90s = ind_90s+1;
%     elseif cond_order(i) >18 && cond_order(i) <= 21
%         cur_wavpath=sound_120s{ind_120s};
%         ind_120s = ind_120s+1;
%     elseif cond_order(i) >21 && cond_order(i) <= 24
%         cur_wavpath=sound_150s{ind_150s};
%         ind_150s = ind_150s+1;
    end
    
    % load (and vocode) sound
    if mod(cond_order(i),3)==1 % for unvocoded condition (1, 4, 7, 10, 13, 16, 19, 22)
        sound_to_present{i} = o_ptb.stimuli.auditory.Wav(cur_wavpath);
        
    elseif mod(cond_order(i),3)==2 %for 7-channel vocoded condition (2, 5, 8, 11, 14, 17, 20, 23)
        sound_to_present{i} = o_ptb.stimuli.auditory.Wav(cur_wavpath);
        sound_to_present{i}.vocode(7, vocode_bw);
        
    elseif mod(cond_order(i),3)==0 %for 3-channel vocoded condition (3, 6, 9, 12, 15, 18, 21, 24)
        sound_to_present{i} = o_ptb.stimuli.auditory.Wav(cur_wavpath);
        sound_to_present{i}.vocode(3, vocode_bw);
    end
    [w, x] = sscanf(cur_wavpath, '%c');
    new_sound_array(i)= str2double(w(x-5:x-4));
    
    targetwords_present{i} = targetwords{new_sound_array(i)};
end


instruction_1 = '實驗即將開始';
instruction_2 = '實驗開始後請保持身體靜止不動';
ptb.screen('TextSize', 80);
xp = functions.h_centered_mandarin(ptb, instruction_1);
ptb.screen('DrawText', double(instruction_1), xp, ptb.win_rect(4)/2-100);
xp = functions.h_centered_mandarin(ptb, instruction_2);
ptb.screen('DrawText', double(instruction_2), xp, ptb.win_rect(4)/2+100);
ptb.flip();

fprintf('\n Press Space to start recording \n');
KbWait();
timestamp = ptb.flip();
WaitSecs('UntilTime', timestamp + 1);

%% play sounds with trigger
for i=ind_start:ind_end
    ptb.draw(o_ptb.stimuli.visual.FixationCross());
    timestamp = ptb.flip();
    WaitSecs('UntilTime', timestamp + rand*2+1);
    
    current_audio = sound_to_present{i};
    if ~subj_data.volume_staircase_done
        warning('Bayesian volume matching was not done! Sounds are not individually adjusted...');
        current_audio.rms = 0.01;
    else
        current_audio.rms = subj_data.stims.audio.rms; % Loudness estimated by Bayes
    end %if
    
    if mod(cond_order(i),3)==1 % for unvocoded condition (1, 4, 7, 10, 13, 16, 19, 22)
        trg_start = 0 + new_sound_array(i); % unvocoded cond + stimuli nr. (1-24)
    elseif mod(cond_order(i),3)==2 % for 7-channel vocoded condition (2, 5, 8, 11, 14, 17, 20, 23)
        trg_start = 30 + new_sound_array(i); % cond. vocode ch. 7 + stimuli nr. (31-54)
    elseif mod(cond_order(i),3)==0 %for 3-channel vocoded condition (3, 6, 9, 12, 15, 18, 21, 24)
        trg_start = 70 + new_sound_array(i); % cond. vocode ch. 3 + stimuli nr. (71-94)
    end
    
    ptb.prepare_audio(current_audio);
    ptb.schedule_audio;
    ptb.play_without_flip;
    
    % start of sound
    if MEG_trigger
        io64(ioObj2,lpt2,trg_start);
        WaitSecs(0.005)
        io64(ioObj2,lpt2,0);
    end
    
    WaitSecs(current_audio.duration + 0.1);
    
    % end of sound
    if MEG_trigger
        io64(ioObj2,lpt2,99);
        WaitSecs(0.005)
        io64(ioObj2,lpt2,0);
    end
    
    % question
    if target_side(i) ==1
        which_word = [targetwords_present{i},'    ', distractorwords{i}];
    else
        which_word = [distractorwords{i},'    ', targetwords_present{i}];
    end
    
    ptb.screen('TextSize', 80);
    xp = functions.h_centered_mandarin(ptb, which_word);
    ptb.screen('DrawText', double(which_word), xp, ptb.win_rect(4)/2);
    timestamp = ptb.flip();
    
    if MEG_trigger % MEG pad response collection
        s = GetSecs;
        s0 = s;
        pressed=0;
        while (pressed~=LH) && (pressed~=RH) && (s-s0<1)
            pressed = io64(ioObj1,lpt1); % MEG response pad trigger
            s = GetSecs;
        end
        
        if pressed == RH
            response{i} = 'right';
        else
            response{i} = 'left';
        end
        pressed = 0;
    else
        response{i}=ptb.wait_for_keys({'left', 'right'}, timestamp+8);
    end
    
    % judge whether the response is correct
    if target_side(i) ==1
        if strcmp(response{i},'left')
            trg_response = 27; % as correct response
        elseif strcmp(response{i},'right')
            trg_response = 28; % as incorrect response
        else
            trg_response = 29; % as miss response
        end
    else
        if strcmp(response{i},'right')
            trg_response = 27;
        elseif strcmp(response{i},'left')
            trg_response = 28;
        else
            trg_response = 29;
        end
    end
    
    ptb.play_on_flip;
    timestamp = ptb.flip();
    WaitSecs('UntilTime', timestamp + 1);
    
    
    % next trial
    instruction_1 = '請按任一鍵進入下一題';
    ptb.screen('TextSize', 80);
    % ptb.screen('TextFont', 'STSong');
    xp = functions.h_centered_mandarin(ptb, instruction_1);
    ptb.screen('DrawText', double(instruction_1), xp, ptb.win_rect(4)/2)
    ptb.flip();
    
    if MEG_trigger
        s = GetSecs;    s0 = s;     pressed=0;
        while (pressed~=RH) && (pressed~=LH) && (s-s0<1)
            s = GetSecs;
            pressed = io64(ioObj1,lpt1);
        end
        timestamp = ptb.flip();
        WaitSecs('UntilTime', timestamp + 1);
    else
        ptb.wait_for_keys({'next'});
        timestamp = ptb.flip();
        WaitSecs('UntilTime', timestamp + 1);
    end
    
    % save in txt file
    fprintf(fid,'%i\t%i\t%i\t%i\t%s\t%s\t%i\n',...
        i,cond_order(i),new_sound_array(i),trg_start,targetwords_present{i},distractorwords{i},trg_response);
    
    % save in subj_data
    trg_start_log{i} = trg_start;
    trg_response_log{i} = trg_response;
    targetwords_log{i} = targetwords_present{i};
end

%%
instruction_1 = sprintf('第 %d. 回合完成! (共6回合)', blk_n);
ptb.screen('TextSize', 80);
xp = functions.h_centered_mandarin(ptb, instruction_1);
ptb.screen('DrawText', double(instruction_1), xp, ptb.win_rect(4)/2);
ptb.flip();
WaitSecs(2);

%% Goodbye
% subj_data.vocoding_attw_pilot.vocoding_order.ind_15s = ind_15s;
% subj_data.vocoding_attw_pilot.vocoding_order.ind_30s = ind_30s;
% subj_data.vocoding_attw_pilot.vocoding_order.ind_60s = ind_60s;
% subj_data.vocoding_attw_pilot.vocoding_order.ind_90s = ind_90s;
% subj_data.vocoding_attw_pilot.vocoding_order.ind_120s = ind_120s;
% subj_data.vocoding_attw_pilot.vocoding_order.ind_150s = ind_150s;

subj_data.vocoding_attw_pilot.vocoding_order.new_sound_array = new_sound_array;
subj_data.vocoding_attw_pilot.triggers.trg_start_log = trg_start_log;
subj_data.vocoding_attw_pilot.triggers.trg_response_log = trg_response_log;
subj_data.vocoding_attw_pilot.triggers.targetwords_log = targetwords_log;
subj_data.vocoding_attw_pilot_block_done = blk_n;
if subj_data.vocoding_attw_pilot_block_done == 6
    subj_data.vocoding_attw_pilot_done = true;
end
save(fullfile(cfg.data_path, subject_id), 'subj_data');
fclose(fid);
sca