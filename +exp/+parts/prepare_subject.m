function prepare_subject(subject_id, MEG_mode)

cfg = exp.init.prepare_cfg();
if ~exist(cfg.data_path)
    mkdir(cfg.data_path);
end %if

if exist(fullfile(cfg.data_path, [subject_id '.mat']))
    fprintf('The Subject is already prepared. You can go on\n');
    return;
end %if

subj_data.MEG_trigger = MEG_mode;

subj_data.stims = exp.init.prepare_stims(cfg);

subj_data.volume_staircase_done = false;
subj_data.resting_done = false;
subj_data.vocoding_attw_pilot_done = false;
subj_data.vocoding_attw_pilot_block_done = 0;

save(fullfile(cfg.data_path, subject_id), 'subj_data');

end

