function cfg = prepare_cfg()
%PREPARE_CFG Summary of this function goes here
%   Detailed explanation goes here
cfg = [];
cfg.data_path = 'data';

% path to experiment 
load('home_path.mat');
cfg.home_path = home_path;

%Path to all soundfiles
cfg.audiobook = '/audiobook/';

%Path to vocoding order
cfg.order = '/vocoding_order/';

% add volume after volume bayse
cfg.add_db = 40;

end

