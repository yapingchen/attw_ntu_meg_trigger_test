function ptb_config = config_ptb(MEG_mode)
%% init paths....
functions.init_o_ptb_paths;

%% create ptb_config
ptb_config = o_ptb.PTB_Config();
ptb_config.draw_borders_sbg = false;
if MEG_mode
    ptb_config.fullscreen = true;
    ptb_config.skip_sync_test = false;
    ptb_config.hide_mouse = true;
else
    ptb_config.window_scale = 0.3; 
    ptb_config.fullscreen = false;
    ptb_config.skip_sync_test = true;
    ptb_config.hide_mouse = false;
end

ptb_config.defaults.text_size = 70;
ptb_config.defaults.fixcross_size = 240;

% button config for volue bayes
ptb_config.keyboardresponse_config.button_mapping('yes') = KbName('RightArrow');
ptb_config.keyboardresponse_config.button_mapping('no') = KbName('LeftArrow');

% button config for vocoding
ptb_config.keyboardresponse_config.button_mapping('next') = KbName('space');
ptb_config.keyboardresponse_config.button_mapping('left') = KbName('LeftArrow');
ptb_config.keyboardresponse_config.button_mapping('right') = KbName('RightArrow');


end

