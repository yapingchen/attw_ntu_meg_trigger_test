% This script is for checking whether Mandarin characters can be well
% presented. It will show two two-character words with some spaces between
% them in the center of the display.
% Just run the script; end it with the space key. 
% If not work well, try using different 'TextFont' first.

commandwindow;
% get cfg
cfg = exp.init.prepare_cfg;
MEG_mode = false;
% init ptb
ptb_config = exp.init.config_ptb(MEG_mode);
ptb = exp.init.init_ptb(ptb_config);
ptb.setup_screen;
ptb.setup_response;

% present instruction
text = ['神經','    ','科學'];
ptb.screen('TextSize', 80);
ptb.screen('TextFont', 'STSong');
xp = functions.h_centered_mandarin(ptb, text);

ptb.screen('DrawText', double(text), xp, ptb.win_rect(4)/2);

% press the space key to end it
ptb.flip();
ptb.wait_for_keys('next');
timestamp = ptb.flip();
WaitSecs('UntilTime', timestamp + 1)

sca