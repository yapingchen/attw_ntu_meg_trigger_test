%% clear, cd & init
clc; clear all; close all;

% initialize basic settings
MEG_mode = false; % if with NTU MEG, set it TRUE
exp.init.config_ptb(MEG_mode);

%% set variables
subject_id = '20001122ABCD';

% prepare subject
exp.parts.prepare_subject(subject_id, MEG_mode);

%% do audiobook blocks trigger test 
exp.parts.vocoding_attw_trigger_test(subject_id, 4); 
% 4 blocks in total. 4 trials in each block (same as real experiment.)
% In the 1st and 2nd blocks, sound files are 15 secs long. 
% In the 3rd block, the first sound is also 15 secs; the others are 30 secs.
% In the 4th block, the first 3 sound files are 60 secs long. The last is 90 secs long. 
% The total duration is most similar to one block in the real experiment (~5 mins).
% You can start with any block here.

