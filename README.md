This script is for trigger time-delay test in the vocoded audiobook experiment. 
The overall structure is similar to the Taiwan_Austria_2020 scripts, but the fool-proof mechanism is removed.
- No need to do volume staircase before the experiment.
- You are allowed to run certain block as many times as you want.
- The trial is not random.
    - Block 1: 4 trials are 15-sec
    - Block 2: same as Block 1
    - Block 3: 1st trial is 15-sec; the others are 30-sec
    - Block 4: the first 3 trials are 60-sec; the last is 90-sec. 
    - This length of Block 4 is most similar to the one in the real experiment.
